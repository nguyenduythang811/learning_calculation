var request = require('supertest'); 
var app = require('../app.js');
var express = require('express');
var bodyParser = require('body-parser');

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

// describe('GET /', function() {   it("should return home page", function(done) {    
//         // The line below is the core test of our app.     
//         request(app).get('/')
//         .expect("Content-type",/json/)
//         .expect(200) // THis is HTTP response
//         .end(function(err,res){
//             // HTTP status should be 200
//             //res.status.should.equal(200);
//             //res.status.should.equal(200);
//             // Error key should be false.
//             //res.body.error.should.equal(false);
//             if(res.status == 200){
//                 done();   
//             }
//             else{
//                 throw err;
//             }
//         });
//     });
// });

describe('POST /tinh', function() {   it("should return number to calculate", function(done) {    
    // The line below is the core test of our app.     
    // var app = express();

    // app.use(express.json());

    // app.post('/', function(req, res){
    //   res.send({num1: 20, num2: 10, pheptinh: '+'});
    // });

    // var app = express()
    // app.use(bodyParser.json())
    // app.use(bodyParser.urlencoded({ extended: false }))

    request(app)
    .post('/tinh')
    .type('form')
    .send({num1 : 10, num2 : 20, pheptinh: '+'})
    .expect("Content-type",/json/)
    .expect(200)
    .end((err, res) => {
        console.log(res.text);
        console.log(res.body);
        done();
    });

    // request(app)
    // .post('/tinh')
    // //.send({num1 : 10, num2 : 20, pheptinh: '+'})
    // .expect("Content-type",/json/)
    // .expect(200) // THis is HTTP response
    // .end(function(err,res){
    //     // HTTP status should be 200
    //     //res.status.should.equal(200);
    //     //res.status.should.equal(200);
    //     // Error key should be false.
    //     //res.body.error.should.equal(false);
    //     console.log(res);
    //     done();
    // });
});
});